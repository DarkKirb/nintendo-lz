# nintendo\_lz

A small rust crate for compressing and decompressing in Nintendo's LZSS formats.

## Contents

This crate contains a library and an application called "lz11". The Library's documentation can be found [here](https://darkkirb.gitlab.io/nintendo-lz/nintendo_lz/). The program's usage can be displayed by passing --help to it.

## Authors

- Charlotte D. - [Dark Kirb](https://gitlab.com/DarkKirb)

## License

This project is licensed under the 2-clause BSD license. See LICENSE for details.
