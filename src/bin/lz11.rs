extern crate clap;
extern crate nintendo_lz;
use std::fs::File;
use std::io::{Read, Write};
use clap::{Arg, App, SubCommand};

fn read_to_vec(src: &mut Read, dest: &mut Vec<u8>) -> std::io::Result<usize> {
    dest.clear();
    loop {
        let mut buf: [u8; 1024] = [0; 1024];
        let size = src.read(&mut buf)?;
        if size < 1024 {
            dest.extend_from_slice(&buf[..size]);
            break;
        }
        dest.extend_from_slice(&buf);
    }
    Ok(dest.len())
}

fn main() {
    let matches = App::new("LZ11 (de)compressor")
        .version("0.1")
        .author("Charlotte D. <darkkirb@gmail.com>")
        .about("Compresses and decompresses files using Nintendo's LZ11 compression.")
        .arg(Arg::with_name("decompress")
             .short("d")
             .long("decompress")
             .help("Decompresses file (as opposed to compress)"))
        .arg(Arg::with_name("INPUT")
             .help("Input file")
             .required(true)
             .index(1))
        .arg(Arg::with_name("OUTPUT")
             .help("Output file")
             .required(true)
             .index(2))
        .get_matches();

    let decompress = match matches.occurrences_of("decompress") {
        0 => false,
        _ => true
    };
    let mut inf = File::open(matches.value_of("INPUT").unwrap()).unwrap();
    let mut outf = File::create(matches.value_of("OUTPUT").unwrap()).unwrap();
    if decompress {
        let decompressed = nintendo_lz::decompress(&mut inf).unwrap();
        outf.write(&decompressed).unwrap();
    } else {
        let mut decompressed: Vec<u8> = Vec::new();
        read_to_vec(&mut inf, &mut decompressed).unwrap();
        nintendo_lz::compress(&decompressed, &mut outf, nintendo_lz::CompressionLevel::LZ11(65809)).unwrap();
    }
}
