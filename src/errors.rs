use std::error;
use std::fmt;
use std::result;

pub type Result<T> = result::Result<T, Box<error::Error>>;

macro_rules! make_err {
    ($name:ident) => {
        #[derive(Debug, Clone)]
        pub struct $name {
            msg: String,
        }
        impl $name {
            pub fn new(msg: &str) -> $name {
                $name {
                    msg: msg.to_string(),
                }
            }
        }
        impl fmt::Display for $name {
            fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
                write!(f, "{}", self.msg)
            }
        }
        impl error::Error for $name {
            fn description(&self) -> &str {
                &self.msg
            }
            fn cause(&self) -> Option<&error::Error> {
                None
            }
        }
    };
}

make_err!(InvalidMagicNumberError);
make_err!(OutOfRangeError);
