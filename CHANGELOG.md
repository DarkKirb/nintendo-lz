# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## 0.1.3 - 2019-03-01
### Fixes
- Makes nintendo_lz compile on non-nightly targets (#1)

### Other
- Added CI checks for beta and release as well

### Deprecation
- The entire API is going to be replaced in version 0.2.

## 0.1.2 - 2018-07-21
### Removed

- Debug output when extracting files

### Fixes

- Fixed a size overflow bug for files bigger than 4GB (on 64 bit machines)

## 0.1.0 - 2018-07-21
### Added

- the library functions decompress(), decompress\_arr(), compress(), compress\_arr()
